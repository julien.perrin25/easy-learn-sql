# supprime le fichier de BDD s'il existe
if (Test-Path "./base/ma_base.db") {
    Remove-Item ./base/ma_base.db
} 

# Lance sqlite, créée une BDD nommée ma_base, créée les tables et enregistre les données
./sqlite3.exe base/ma_base.db  ".read requetes/creer_tables.sql"
./sqlite3.exe base/ma_base.db  ".read requetes/enregistrer_donnees.sql"