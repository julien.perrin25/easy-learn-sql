# Easy learn SQL

A simple tool that should help you learn SQL on Windows using SQLite.

![You can run queries in your file `requetes/mes_selects.sql` by using commange `Ctrl+Shift+Q`](https://gitlab.com/julien.perrin25/easy-learn-sql/raw/master/run_query.png)

## Installation

To use this tool, you have to download **_sqlite3.exe_** on [SQLite Website](https://sqlite.org/download.html) and copy it into racine folder.

It is suggested to install Visual Studio Code as an IDE : https://code.visualstudio.com/Download

You can also install package **_SQL Server (mssql)_** on Visual Studio Code which is useful to quickly run querries on database.

## Use the tool

Put the tables you want to create into file : `requetes/creer_tables.sql`

Add the INSERT querries to run in : `requetes/enregistrer_donnees.sql`

Once it is done, you can run `lancer_BDD.ps1` to create the database with tables created and lines inserted. \
Running again `lancer_BDD.ps1` will reload database. \
`supprimer_BDD.ps1` will delete database.

Once the database has been created, if you installed the package **_SQL Server (mssql)_**, you can right click on file `ma_base.db`, and select `open datase` to visualise database.

You can also run queries in your file `requetes/mes_selects.sql` by using commange `Ctrl+Shift+Q`
